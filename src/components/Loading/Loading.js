import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Loading() {
    const { isLoading } = useSelector((state) => state.LoadingReducer);

    return (
        <Fragment>
            {isLoading ? (
                <div className="fixed w-screen h-screen bg-opacity-75 bg-white z-50 flex items-center justify-center">
                    <RingLoader
                        color="#ef0303"
                        cssOverride={{}}
                        loading
                        speedMultiplier={1}
                        size={100}
                    />
                </div>
            ) : (
                ""
            )}
        </Fragment>
    );
}
