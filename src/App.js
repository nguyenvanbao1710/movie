import "./App.css";
import "antd/dist/antd.min.css";
import { Switch } from "react-router";
import { HomeTemplate } from "./templates/HomeTemplate/HomeTemplate";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Detail from "./pages/Detail/Detail";
import Checkout from "./pages/Checkout/Checkout";
import CheckoutTemplate from "./templates/CheckoutTemplate/CheckoutTemplate";
import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
import { BrowserRouter as Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import Loading from "./components/Loading/Loading";
import Account from "./pages/Account/Account";
import AdminTemplate from "./templates/AdminTemplate/AdminTemplate";
import Films from "./pages/Admin/Films/Films";
import ShowTime from "./pages/Admin/ShowTime/ShowTime";
import Users from "./pages/Admin/Users/Users";
import AddNew from "./pages/Admin/Films/AddNew/AddNew";
import Edit from "./pages/Admin/Films/Edit/Edit";
export const history = createBrowserHistory();
function App() {
    return (
        <>
            <Loading />
            <Router history={history}>
                <Switch>
                    <HomeTemplate exact path="/account" Component={Account} />
                    <HomeTemplate exact path="/detail/:id" Component={Detail} />
                    <HomeTemplate path="/" exact Component={Home} />
                    <CheckoutTemplate
                        exact
                        path="/checkout/:id"
                        Component={Checkout}
                    />
                    <UserTemplate path="/login" exact Component={Login} />
                    <UserTemplate path="/register" exact Component={Register} />
                    <AdminTemplate
                        path="/admin/films"
                        exact
                        Component={Films}
                    />
                    <AdminTemplate
                        path="/admin/films/addnew"
                        exact
                        Component={AddNew}
                    />
                    <AdminTemplate
                        path="/admin/films/edit/:id"
                        exact
                        Component={Edit}
                    />
                    <AdminTemplate
                        path="/admin/films/showtime/:id"
                        exact
                        Component={ShowTime}
                    />
                    <AdminTemplate
                        path="/admin/users"
                        exact
                        Component={Users}
                    />
                </Switch>
            </Router>
        </>
    );
}

export default App;
