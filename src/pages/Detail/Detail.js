import { Progress, Rate, Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import { layThongTinChiTietPhim } from "../../redux/actions/QuanLyRapAction";
import "./detail.css";
export default function Detail() {
    const { filmDetail } = useSelector((state) => state.MovieReducer);
    let { id } = useParams();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(layThongTinChiTietPhim(id));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="detail_page pt-20">
            <div className="grid lg:grid-cols-12 xs:grid-cols-2 xs:px-5">
                <div className="col-span-4 lg:col-start-4">
                    <div className="grid grid-cols-2">
                        <div className="shadow-2xl">
                            <img
                                src={filmDetail.hinhAnh}
                                alt=""
                                style={{ height: "100%", width: "100%" }}
                            />
                        </div>
                        <div className="pl-6">
                            <p className="text-xl font-bold">
                                {filmDetail.tenPhim}
                            </p>
                            <p className="text-base py-4 ">
                                {filmDetail.moTa?.slice(0, 100)} . . .
                            </p>
                            <p className="text-base text-red-600 font-bold mb-4">
                                {moment(filmDetail.ngayKhoiChieu).format(
                                    "DD.MM.YYYY"
                                )}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col-span-4 pl-10 w-full xs:hidden lg:block">
                    <div>
                        <Progress
                            type="circle"
                            strokeColor={{
                                "0%": "#108ee9",
                                "100%": "#87d068",
                            }}
                            percent={filmDetail.danhGia * 10}
                        />
                    </div>
                    <div className="pt-2">
                        <Rate
                            disabled
                            allowHalf
                            value={filmDetail.danhGia / 2}
                        />
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-12 pb-20 pt-10">
                <div className="lg:col-span-4 xs:col-span-12 w-full lg:col-start-4 lg:col-end-10 bg-white shadow-2xl py-4">
                    <Tabs centered>
                        <Tabs.TabPane tab="Lịch chiếu" key={1}>
                            <div>
                                <Tabs tabPosition="left">
                                    {filmDetail.heThongRapChieu?.map(
                                        (htr, index) => {
                                            return (
                                                <TabPane
                                                    tab={
                                                        <div>
                                                            <img
                                                                src={htr.logo}
                                                                alt=""
                                                                width={35}
                                                                height={35}
                                                            />
                                                        </div>
                                                    }
                                                    key={index}
                                                >
                                                    <p className="lg:text-xl xs:text-sm md:text-base   font-semibold text-green-600 lg:pr-36 xs:pr-0">
                                                        {htr.cumRapChieu?.map(
                                                            (cumRap, index) => {
                                                                return (
                                                                    <div
                                                                        key={
                                                                            index
                                                                        }
                                                                    >
                                                                        <p>
                                                                            {
                                                                                cumRap.tenCumRap
                                                                            }
                                                                        </p>
                                                                        <p className="text-gray-500 lg:text-base xs:text-xs">
                                                                            {
                                                                                cumRap.diaChi
                                                                            }
                                                                        </p>
                                                                        <div className="grid sm:grid-cols-4   xs:grid-cols-2">
                                                                            {cumRap.lichChieuPhim
                                                                                ?.slice(
                                                                                    0,
                                                                                    4
                                                                                )
                                                                                .map(
                                                                                    (
                                                                                        lichChieu,
                                                                                        index
                                                                                    ) => {
                                                                                        return (
                                                                                            <div
                                                                                                className="col-span-1 py-2"
                                                                                                key={
                                                                                                    index
                                                                                                }
                                                                                            >
                                                                                                <NavLink
                                                                                                    to={`/checkout/${lichChieu.maLichChieu}`}
                                                                                                    className="text-red-600 md:text-sm xs:text-xs border border-red-400 py-1 px-2 hover:text-red-500 bg-white hover:bg-green-500"
                                                                                                >
                                                                                                    {moment(
                                                                                                        lichChieu.ngayChieuGioChieu
                                                                                                    ).format(
                                                                                                        "hh:mm A"
                                                                                                    )}
                                                                                                </NavLink>
                                                                                            </div>
                                                                                        );
                                                                                    }
                                                                                )}
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }
                                                        )}
                                                    </p>
                                                </TabPane>
                                            );
                                        }
                                    )}
                                </Tabs>
                            </div>
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="Thông tin" key={2}>
                            <div className="px-5">
                                <div className="text-lg font-medium pb-2">
                                    {filmDetail.tenPhim}
                                </div>
                                <div>{filmDetail.moTa}</div>
                            </div>
                        </Tabs.TabPane>
                    </Tabs>
                </div>
            </div>
        </div>
    );
}
