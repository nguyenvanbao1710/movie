import { Tabs } from "antd";
import _ from "lodash";
import moment from "moment";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
    datVeAction,
    getListPhongVeAction,
} from "../../redux/actions/QuanLyDatVeAction";
import { layThongTinNguoiDungAction } from "../../redux/actions/QuanLyNguoiDungAction";
import { DAT_VE } from "../../redux/constants/quanLyDatVeConstants";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import "./checkout.css";
function Checkout() {
    const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
    const { chiTietPhongVe, danhSachGheDangDat } = useSelector(
        (state) => state.QuanLyDatVeReducer
    );
    const dispatch = useDispatch();
    const { id } = useParams();
    useEffect(() => {
        dispatch(getListPhongVeAction(id));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const { thongTinPhim, danhSachGhe } = chiTietPhongVe;
    const renderSeats = () => {
        return danhSachGhe.map((ghe, index) => {
            let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
            let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
            let classGheDangDat = "";
            let indexGheDD = danhSachGheDangDat.findIndex(
                (gheDD) => gheDD.maGhe === ghe.maGhe
            );
            if (indexGheDD !== -1) {
                classGheDaDat = "gheDangDat";
            }
            return (
                <Fragment>
                    <button
                        onClick={() => {
                            dispatch({
                                type: DAT_VE,
                                gheDuocChon: ghe,
                            });
                        }}
                        disabled={ghe.daDat}
                        className={`ghe ${classGheVip} ${classGheDaDat} ${classGheDangDat}`}
                        key={index}
                    >
                        {ghe.daDat ? <div className="text-lg">x</div> : ghe.stt}
                    </button>
                    {(index + 1) % 16 === 0 ? <br /> : ""}
                </Fragment>
            );
        });
    };

    return (
        <div className="pb-10 checkout text-white">
            <div className="grid lg:grid-cols-12   ">
                <div className="lg:col-span-8  xs:pl-4 ">
                    <div className="flex  justify-center mt-5">
                        <div>{renderSeats()}</div>
                    </div>
                    <div className="text-center flex justify-center mt-5">
                        <table className=" xs:text-xs lg:w-1/2">
                            <thead>
                                <tr>
                                    <th>Ghế đang đặt</th>
                                    <th className="xs:px-3 md:px-5">
                                        Ghế đã đặt
                                    </th>
                                    <th>Ghế thường</th>
                                    <th className="xs:pl-3 md:pl-5">Ghế vip</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <button className="ghe gheDangDat">
                                            00
                                        </button>
                                    </td>
                                    <td>
                                        <button className="ghe gheDaDat">
                                            X
                                        </button>
                                    </td>
                                    <td>
                                        <button className="ghe">00</button>
                                    </td>
                                    <td>
                                        <button className="ghe gheVip">
                                            00
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="xs:mt-10 xs:mx-10  lg:col-span-3 lg:col-start-9 lg:col-end-12 lg:mt-5 sm:mx-20 lg:mx-0">
                    <h3 className="text-green-400 text-center text-2xl font-bold">
                        ĐẶT VÉ NGAY
                    </h3>
                    <h3 className="text-xl text-white font-bold my-2">
                        Phim: {thongTinPhim.tenPhim}
                    </h3>
                    <p>
                        Địa điểm: {thongTinPhim.tenCumRap} -{" "}
                        {thongTinPhim.tenRap}
                    </p>
                    <p className="border-b-2 pb-2">
                        Ngày chiếu: {thongTinPhim.ngayChieu} -{" "}
                        {thongTinPhim.gioChieu}
                    </p>

                    <div className="flex flex-row my-5 border-b-2">
                        <div className="w-4/5">
                            <span>Tổng tiền:</span>
                        </div>
                        <div className="text-right col-span-1">
                            <span className="text-yellow-300 text-lg">
                                {danhSachGheDangDat
                                    .reduce((tongTien, ghe, index) => {
                                        return (tongTien += ghe.giaVe);
                                    }, 0)
                                    .toLocaleString()}
                                đ
                            </span>
                        </div>
                    </div>
                    <div className="border-b-2">
                        <span className="text-white text-lg font-medium ">
                            Chọn:
                            {_.sortBy(danhSachGheDangDat, "stt").map(
                                (gheDD, index) => {
                                    return (
                                        <span
                                            key={index}
                                            className="text-yellow-300 text-base mx-1"
                                        >
                                            Ghế: {gheDD.stt}
                                        </span>
                                    );
                                }
                            )}
                        </span>
                    </div>
                    <div className="my-5 border-b-2">
                        <i>Email:</i>
                        <br />
                        {userLogin.email}
                    </div>
                    <div className="my-5 border-b-2">
                        <i>Phone:</i>
                        <br />
                        {userLogin.soDT}
                    </div>
                    <div
                        onClick={() => {
                            const thongTinDatVe = new ThongTinDatVe();
                            thongTinDatVe.maLichChieu = id;
                            thongTinDatVe.danhSachVe = danhSachGheDangDat;
                            dispatch(datVeAction(thongTinDatVe));
                        }}
                        className="mb-0 flex flex-col justify-end items-center cursor-pointer"
                    >
                        <div className="bg-green-500 hover:bg-green-700 text-white w-full text-center py-3 font-bold text-2xl">
                            ĐẶT VÉ
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function (props) {
    return (
        <div>
            <Tabs defaultActiveKey="1">
                <Tabs.TabPane
                    tab={
                        <div className="sm:px-5  sm:text-base sm:font-medium xs:px-2 xs:text-xs text-red-500">
                            01 CHỌN GHẾ & ĐẶT VÉ
                        </div>
                    }
                    key="1"
                >
                    <Checkout {...props} />
                </Tabs.TabPane>
                <Tabs.TabPane
                    tab={
                        <div className="sm:px-5 xs:px-2 xs:text-xs  sm:text-base sm:font-medium text-red-500">
                            02 LỊCH SỬ ĐẶT VÉ
                        </div>
                    }
                    key="2"
                >
                    <KetQuaDatVe {...props} />
                </Tabs.TabPane>
            </Tabs>
        </div>
    );
}

function KetQuaDatVe(props) {
    const { thongTinNguoiDung } = useSelector(
        (state) => state.QuanLyNguoiDungReducer
    );
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(layThongTinNguoiDungAction());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const renderHistoryTickets = () => {
        return thongTinNguoiDung.thongTinDatVe
            ?.reverse()
            .map((items, index) => {
                const seats = _.first(items.danhSachGhe);
                return (
                    <div className="p-2 lg:w-1/3 md:w-1/2 w-full" key={index}>
                        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                            <img
                                alt="team"
                                className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
                                src={items.hinhAnh}
                            />
                            <div className="flex-grow">
                                <h2 className="text-gray-900 title-font font-medium">
                                    {items.tenPhim}
                                </h2>
                                <p className="text-gray-500">
                                    {moment(items.ngayDat).format(
                                        "hh:mm A - DD/MM/YYYY"
                                    )}
                                </p>
                                <p className="text-gray-500">
                                    Thời lượng phim: {items.thoiLuongPhim} phút
                                </p>
                                <p className="text-gray-500">
                                    Địa điểm: {seats.tenHeThongRap}
                                </p>
                                <p className="text-gray-500">
                                    Tên rạp: {seats.tenCumRap} - Ghế{" "}
                                    {items.danhSachGhe?.map((ghe, index) => {
                                        return (
                                            <span key={index}>
                                                [{ghe.tenGhe}]{" "}
                                            </span>
                                        );
                                    })}
                                </p>
                            </div>
                        </div>
                    </div>
                );
            });
    };
    return (
        <div>
            <section className="text-gray-600 body-font">
                <div className="container px-5 mx-auto">
                    <div className="flex flex-wrap -m-2">
                        {renderHistoryTickets()}
                    </div>
                </div>
            </section>
        </div>
    );
}
