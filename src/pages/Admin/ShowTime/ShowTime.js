import { Button, DatePicker, Form, InputNumber, message, Select } from "antd";
import { useFormik } from "formik";
import moment from "moment";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { quanLyDatVeServ } from "../../../services/quanLyDatVeServ";
import { quanLyRapService } from "../../../services/quanLyRapService";
import { showTimeSchema } from "../../../utils/validationSchema/formValidatAdminSchema";

export default function ShowTime() {
    const { id } = useParams();
    const formik = useFormik({
        initialValues: {
            maPhim: id,
            ngayChieuGioChieu: "",
            maRap: "",
            giaVe: "",
        },
        onSubmit: async (values) => {
            try {
                const result = await quanLyDatVeServ.taoLichChieu(values);
                message.success("Tạo lịch chiếu thành công!");
            } catch (error) {
                message.error("Hệ thống đang gặp sự cố. Vui lòng thử lại sau!");
            }
        },
        validationSchema: showTimeSchema,
    });
    const [state, setState] = useState({
        heThongRapChieu: [],
        cumRapChieu: [],
    });
    useEffect(() => {
        let thongThongTinHeThongRap = async () => {
            try {
                let result = await quanLyRapService.getInforHeThongRap();
                setState({
                    ...state,
                    heThongRapChieu: result.data.content,
                });
            } catch (error) {}
        };
        thongThongTinHeThongRap();
    }, []);

    const handleChangeHeThongRap = async (values) => {
        try {
            let result = await quanLyRapService.layThongTinCumRap(values);
            setState({
                ...state,
                cumRapChieu: result.data.content,
            });
        } catch (error) {}
    };
    const handleChangeCumRap = (value) => {
        formik.setFieldValue("maRap", value);
    };

    const onOk = (values) => {
        formik.setFieldValue(
            "ngayChieuGioChieu",
            moment(values).format("DD/MM/YYYY hh:mm:ss")
        );
    };

    const onChangeDate = (values) => {
        formik.setFieldValue(
            "ngayChieuGioChieu",
            moment(values).format("DD/MM/YYYY hh:mm:ss")
        );
    };
    const onChangeInputNumber = (value) => {
        formik.setFieldValue("giaVe", value);
    };
    let film = {};
    if (localStorage.getItem("filmParams")) {
        film = JSON.parse(localStorage.getItem("filmParams"));
    }
    return (
        <>
            <div>
                <h3 className="text-xl mb-5 text-red-500 font-medium">
                    Tạo lịch chiếu
                </h3>
            </div>
            <div className="flex justify-between">
                <div className="w-full mt-7">
                    <Form
                        onSubmitCapture={formik.handleSubmit}
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 8,
                        }}
                        layout="horizontal"
                    >
                        <Form.Item label="Hệ thống rạp">
                            <Select
                                options={state.heThongRapChieu?.map(
                                    (htr, index) => ({
                                        label: htr.tenHeThongRap,
                                        value: htr.tenHeThongRap,
                                    })
                                )}
                                onChange={handleChangeHeThongRap}
                                placeholder="Chọn hệ thống rạp"
                            />
                            <span className="text-red-500 text-xs">
                                {formik.errors.heThongRap}
                            </span>
                        </Form.Item>
                        <Form.Item label="Cụm rạp">
                            <Select
                                options={state.cumRapChieu?.map(
                                    (cumRap, index) => ({
                                        label: cumRap.tenCumRap,
                                        value: cumRap.maCumRap,
                                    })
                                )}
                                onChange={handleChangeCumRap}
                                placeholder="Chọn cụm rạp"
                            />
                            <span className="text-red-500 text-xs">
                                {formik.errors.maRap}
                            </span>
                        </Form.Item>
                        <Form.Item label="Ngày chiếu, giờ chiếu">
                            <DatePicker
                                showTime
                                format={"DD/MM/YYYY h:mm:ss"}
                                onChange={onChangeDate}
                                onOk={onOk}
                            />
                            <div className="text-red-500 text-xs">
                                {formik.errors.ngayChieuGioChieu}
                            </div>
                        </Form.Item>
                        <Form.Item label="Giá vé">
                            <InputNumber
                                min={75000}
                                max={150000}
                                onChange={onChangeInputNumber}
                            />
                            <div className="text-red-500 text-xs">
                                {formik.errors.giaVe}
                            </div>
                        </Form.Item>
                        <Form.Item label="Tạo lịch chiếu">
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <div className="w-1/2">
                    <h3 className="text-lg font-medium mb-2">{film.tenPhim}</h3>
                    <img src={film.hinhAnh} width={220} alt="" />
                </div>
            </div>
        </>
    );
}
