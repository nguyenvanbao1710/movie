import React, { useEffect } from "react";
import { DatePicker, Form, Input, InputNumber, Switch } from "antd";
import { useFormik } from "formik";
import moment from "moment";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    getInforMovieAction,
    postCapNhatPhimUpLoadAction,
} from "../../../../redux/actions/QuanLyPhimAction";
import { GROUP_ID } from "../../../../utils/settings/config";
import { useParams } from "react-router-dom";
import { addNewSchema } from "../../../../utils/validationSchema/formValidatAdminSchema";

export default function Edit() {
    const [imgUrl, setImgUrl] = useState("");
    const dispatch = useDispatch();
    const { id } = useParams();
    const { thongTinFilm } = useSelector((state) => state.MovieReducer);
    useEffect(() => {
        dispatch(getInforMovieAction(id));
    }, []);

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            maPhim: thongTinFilm.maPhim,
            tenPhim: thongTinFilm.tenPhim,
            trailer: thongTinFilm.trailer,
            moTa: thongTinFilm.moTa,
            ngayKhoiChieu: thongTinFilm.ngayKhoiChieu,
            dangChieu: thongTinFilm.dangChieu,
            sapChieu: thongTinFilm.sapChieu,
            hot: thongTinFilm.hot,
            danhGia: thongTinFilm.danhGia,
            hinhAnh: null,
        },
        onSubmit: (values) => {
            values.maNhom = GROUP_ID;
            let formData = new FormData();
            for (let key in values) {
                if (key !== "hinhAnh") {
                    formData.append(key, values[key]);
                } else {
                    if (values.hinhAnh !== null) {
                        formData.append(
                            "File",
                            values.hinhAnh,
                            values.hinhAnh.name
                        );
                    }
                }
            }
            dispatch(postCapNhatPhimUpLoadAction(formData));
        },
        validationSchema: addNewSchema,
    });
    const handleChangeDatePicker = (value) => {
        formik.setFieldValue("ngayKhoiChieu", moment(value));
    };
    const handleChangeSwitch = (name) => {
        return (value) => {
            formik.setFieldValue(name, value);
        };
    };
    const handleChangeInputNumber = (name) => {
        return (value) => {
            formik.setFieldValue(name, value);
        };
    };
    const handleChangeFile = async (e) => {
        let file = e.target.files[0];
        if (
            file.type === "image/jpeg" ||
            file.type === "image/gif" ||
            file.type === "image/png"
        ) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setImgUrl(e.target.result);
            };
            await formik.setFieldValue("hinhAnh", file);
        }
    };
    return (
        <div>
            <div>
                <h3 className="text-xl  font-medium text-red-600 mb-3">
                    Cập nhật phim
                </h3>
            </div>
            <div>
                <Form
                    onSubmitCapture={formik.handleSubmit}
                    labelCol={{
                        span: 4,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    layout="horizontal"
                >
                    <Form.Item label="Tên phim">
                        <Input
                            onChange={formik.handleChange}
                            name="tenPhim"
                            value={formik.values.tenPhim}
                        />
                        <span className="text-red-500 text-xs">
                            {formik.errors.tenPhim}
                        </span>
                    </Form.Item>
                    <Form.Item label="Trailer">
                        <Input
                            onChange={formik.handleChange}
                            name="trailer"
                            value={formik.values.trailer}
                        />
                        <span className="text-red-500 text-xs">
                            {formik.errors.trailer}
                        </span>
                    </Form.Item>
                    <Form.Item label="Mô tả">
                        <Input.TextArea
                            onChange={formik.handleChange}
                            name="moTa"
                            value={formik.values.moTa}
                        />
                        <span className="text-red-500 text-xs">
                            {formik.errors.moTa}
                        </span>
                    </Form.Item>
                    <Form.Item label="Hình ảnh">
                        <input type="file" onChange={handleChangeFile} />
                        <img
                            src={imgUrl === "" ? thongTinFilm.hinhAnh : imgUrl}
                            alt=".."
                            width={50}
                            height={60}
                            className="mt-2"
                        />
                    </Form.Item>
                    <Form.Item label="Ngày khởi chiếu">
                        <DatePicker
                            format={"DD/MM/YYYY"}
                            onChange={handleChangeDatePicker}
                            value={moment(formik.values.ngayKhoiChieu)}
                        />
                        <div className="text-red-500 text-xs">
                            {formik.errors.ngayKhoiChieu}
                        </div>
                    </Form.Item>
                    <Form.Item label="Đánh giá">
                        <InputNumber
                            min={1}
                            max={10}
                            onChange={handleChangeInputNumber("danhGia")}
                            value={formik.values.danhGia}
                        />
                    </Form.Item>
                    <Form.Item label="Đang chiếu">
                        <div id="switch_addNew">
                            <Switch
                                className=" bg-gray-300"
                                onChange={handleChangeSwitch("dangChieu")}
                                checked={formik.values.dangChieu}
                            />
                        </div>
                    </Form.Item>
                    <Form.Item label="Sắp chiếu">
                        <div id="switch_addNew">
                            <Switch
                                className=" bg-gray-300"
                                onChange={handleChangeSwitch("sapChieu")}
                                checked={formik.values.sapChieu}
                            />
                        </div>
                    </Form.Item>
                    <Form.Item label="Hot">
                        <div id="switch_addNew">
                            <Switch
                                className=" bg-gray-300"
                                onChange={handleChangeSwitch("hot")}
                                checked={formik.values.hot}
                            />
                        </div>
                    </Form.Item>
                    <Form.Item label="Cập nhật">
                        <button
                            type="submit"
                            className="border py-1 px-5  border-blue-400 hover:bg-blue-400 hover:text-white rounded"
                        >
                            Submit
                        </button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
