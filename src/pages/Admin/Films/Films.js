import React, { Fragment, useRef, useState } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Popconfirm, Space, Table, Tooltip } from "antd";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
    getDanhSachPhimAction,
    xoaPhimAction,
} from "../../../redux/actions/QuanLyPhimAction";
import { NavLink } from "react-router-dom";

export default function Films() {
    const { arrMovie } = useSelector((state) => state.MovieReducer);
    const dispatch = useDispatch();
    const [searchText, setSearchText] = useState("");
    const [searchedColumn, setSearchedColumn] = useState("");
    const searchInput = useRef(null);
    useEffect(() => {
        dispatch(getDanhSachPhimAction);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };
    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText("");
    };
    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{
                        marginBottom: 8,
                        display: "block",
                    }}
                />
                <Space>
                    <Button
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() =>
                            clearFilters && handleReset(clearFilters)
                        }
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? "#1890ff" : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: "#ffc069",
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleDelete = (e) => {
        dispatch(xoaPhimAction(e));
    };

    const columns = [
        {
            title: "Mã phim",
            dataIndex: "maPhim",
            key: "maPhim",
            width: "10%",
            ...getColumnSearchProps("maPhim"),
            sorter: (a, b) => a.maPhim - b.maPhim,
            sortDirections: ["descend", "ascend"],
        },
        {
            title: "Tên phim",
            dataIndex: "tenPhim",
            key: "tenPhim",
            width: "25%",
            ...getColumnSearchProps("tenPhim"),
            sorter: (a, b) => {
                let tenPhimA = a.tenPhim.toLowerCase().trim();
                let tenPhimB = b.tenPhim.toLowerCase().trim();
                if (tenPhimA > tenPhimB) {
                    return 1;
                }
                return -1;
            },
            sortDirections: ["descend", "ascend"],
        },
        {
            title: "Hình ảnh",
            dataIndex: "hinhAnh",
            key: "hinhAnh",
            width: "10%",
            render: (text, film) => {
                return (
                    <Fragment>
                        <img
                            src={film.hinhAnh}
                            alt={film.tenPhim}
                            width={70}
                            height={100}
                        />
                    </Fragment>
                );
            },
        },
        {
            title: "Mô tả",
            dataIndex: "moTa",
            key: "moTa",
        },
        {
            title: "Thao tác",
            dataIndex: "action",
            key: "action",
            width: "10%",
            render: (text, film) => {
                return (
                    <Fragment>
                        <div className="flex items-center">
                            <NavLink
                                key={1}
                                to={`/admin/films/edit/${film.maPhim}`}
                                className="text-base text-blue-500"
                            >
                                <i className="fa-solid fa-pen-to-square"></i>
                            </NavLink>
                            <NavLink
                                key={3}
                                to={`/admin/films/showtime/${film.maPhim}`}
                                onClick={() => {
                                    localStorage.setItem(
                                        "filmParams",
                                        JSON.stringify(film)
                                    );
                                }}
                                className="text-base px-4 text-green-500 hover:text-green-400"
                            >
                                <Tooltip
                                    placement="top"
                                    title={"Tạo lịch chiếu"}
                                >
                                    <i className="fa-solid fa-calendar-days"></i>
                                </Tooltip>
                            </NavLink>
                            <Popconfirm
                                title="Bạn có chắc muốn xoá phim này không?"
                                placement="left"
                                onConfirm={() => {
                                    handleDelete(film.maPhim);
                                }}
                            >
                                <span
                                    key={2}
                                    className="text-base text-red-500  hover:text-red-400"
                                >
                                    <i className="fa-solid fa-trash-can"></i>
                                </span>
                            </Popconfirm>
                        </div>
                    </Fragment>
                );
            },
        },
    ];
    return (
        <>
            <div className="flex justify-between">
                <h3 className="text-xl  font-medium text-red-600 mb-5">
                    Quản Lý Phim
                </h3>
                <div className="pr-5">
                    <NavLink
                        to="/admin/films/addnew"
                        className="border bg-blue-500 text-white px-3 py-2 rounded hover:text-white hover:bg-blue-400"
                    >
                        Thêm phim
                    </NavLink>
                </div>
            </div>
            <Table
                columns={columns}
                dataSource={arrMovie}
                scroll={{
                    y: 480,
                }}
                pagination={{
                    pageSize: 20,
                }}
            />
        </>
    );
}
