import React, { Fragment, useRef, useState } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, message, Popconfirm, Space, Table, Tag } from "antd";
import Highlighter from "react-highlight-words";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { quanLyNguoiDungServ } from "../../../services/quanLyNguoiDungServ";
import { deleteUserAction } from "../../../redux/actions/QuanLyNguoiDungAction";

export default function Users() {
    const dispatch = useDispatch();
    const [userList, setUserList] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [searchedColumn, setSearchedColumn] = useState("");
    const searchInput = useRef(null);
    useEffect(() => {
        quanLyNguoiDungServ
            .getUserList()
            .then((res) => {
                setUserList(res.data.content);
            })
            .catch((err) => {});
    }, []);
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };
    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText("");
    };
    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{
                        marginBottom: 8,
                        display: "block",
                    }}
                />
                <Space>
                    <Button
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() =>
                            clearFilters && handleReset(clearFilters)
                        }
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? "#1890ff" : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: "#ffc069",
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleDelete = (e) => {
        dispatch(deleteUserAction(e));
    };

    const columns = [
        {
            title: "Tài khoản",
            dataIndex: "taiKhoan",
            key: "taiKhoan",
            // width: "15%",
            ...getColumnSearchProps("taiKhoan"),
            sorter: (a, b) => {
                let taiKhoanA = a.hoTen.toLowerCase().trim();
                let taiKhoanB = b.hoTen.toLowerCase().trim();
                if (taiKhoanA > taiKhoanB) {
                    return 1;
                }
                return -1;
            },
            sortDirections: ["descend", "ascend"],
        },
        {
            title: "Họ tên",
            dataIndex: "hoTen",
            key: "hoTen",
            // width: "20%",
            ...getColumnSearchProps("hoTen"),
            sorter: (a, b) => {
                let hoTenA = a.hoTen.toLowerCase().trim();
                let hoTenB = b.hoTen.toLowerCase().trim();
                if (hoTenA > hoTenB) {
                    return 1;
                }
                return -1;
            },
            sortDirections: ["descend", "ascend"],
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "email",
            // width: "20%",
        },
        {
            title: "Số điện thoại",
            dataIndex: "soDT",
            key: "soDT",
        },
        {
            title: "Loại người dùng",
            dataIndex: "maLoaiNguoiDung",
            key: "maLoaiNguoiDung",
            sorter: (a, b) =>
                a.maLoaiNguoiDung.length - b.maLoaiNguoiDung.length,
            sortDirections: ["descend", "ascend"],
            render: (type) => {
                if (type === "QuanTri") {
                    return <Tag color="red">Quản Trị</Tag>;
                } else {
                    return <Tag color="blue">Khách Hàng</Tag>;
                }
            },
        },
        {
            title: "Thao tác",
            dataIndex: "action",
            key: "action",
            width: "10%",
            render: (text, user) => {
                return (
                    <Fragment>
                        <div className="flex items-center">
                            <button
                                onClick={() => {
                                    message.error(
                                        "Hệ thống đang gặp sự cố. Vui lòng thử lại sau!"
                                    );
                                }}
                                className="text-base text-blue-500 pl-2 mr-7"
                            >
                                <i className="fa-solid fa-pen-to-square"></i>
                            </button>

                            <Popconfirm
                                title="Bạn có chắc muốn xoá phim này không?"
                                placement="left"
                                onConfirm={() => {
                                    handleDelete(user.taiKhoan);
                                }}
                            >
                                <span
                                    key={2}
                                    className="text-base text-red-500  hover:text-red-400"
                                >
                                    <i className="fa-solid fa-trash-can"></i>
                                </span>
                            </Popconfirm>
                        </div>
                    </Fragment>
                );
            },
        },
    ];
    return (
        <>
            <div className="flex justify-between">
                <h3 className="text-xl  font-medium text-red-600 mb-5">
                    Quản Lý Tài Khoản
                </h3>
            </div>
            <Table
                columns={columns}
                dataSource={userList}
                scroll={{
                    y: 480,
                }}
            />
        </>
    );
}
