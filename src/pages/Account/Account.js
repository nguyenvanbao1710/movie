import { useFormik } from "formik";
import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    layThongTinNguoiDungAction,
    updateThongTinUserAction,
} from "../../redux/actions/QuanLyNguoiDungAction";
import { GROUP_ID } from "../../utils/settings/config";
import { signInUserSchema } from "../../utils/validationSchema/userValidationSchema";

export default function Account() {
    const { thongTinNguoiDung } = useSelector(
        (state) => state.QuanLyNguoiDungReducer
    );
    useEffect(() => {
        dispatch(layThongTinNguoiDungAction());
    }, []);

    const dispatch = useDispatch();
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            taiKhoan: thongTinNguoiDung.taiKhoan,
            matKhau: thongTinNguoiDung.matKhau,
            email: thongTinNguoiDung.email,
            soDt: thongTinNguoiDung.soDT,
            maNhom: `${GROUP_ID}`,
            maLoaiNguoiDung: `${thongTinNguoiDung.maLoaiNguoiDung}`,
            hoTen: thongTinNguoiDung.hoTen,
        },
        onSubmit: (values) => {
            dispatch(updateThongTinUserAction(values));
        },
        validationSchema: signInUserSchema,
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <div className="h-full">
                <div className="border-b-2 block md:flex">
                    <div className="w-full md:w-2/5 p-4 sm:p-6 lg:p-8 bg-white shadow-md">
                        <div className="flex justify-between">
                            <span className="text-xl font-semibold block">
                                Thông tin cá nhân
                            </span>
                            <button
                                type="submit"
                                href="/"
                                className="-mt-2 text-md font-bold text-white bg-gray-700 rounded-full px-5 py-2 hover:bg-gray-800"
                            >
                                Cập nhật
                            </button>
                        </div>
                        <span className="text-gray-600">
                            Xin chào{" "}
                            <span className="text-lg font-medium">
                                {thongTinNguoiDung.hoTen}
                            </span>{" "}
                            !
                        </span>
                        <div className="w-full p-8 mx-2 flex justify-center">
                            <img
                                id="showImage"
                                className="max-w-xs w-32 items-center border"
                                src="https://picsum.photos/200/200"
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="w-full md:w-3/5 p-8 bg-white lg:ml-4 shadow-md">
                        <div className="rounded  shadow p-6">
                            <div className="pb-4">
                                <label
                                    htmlFor="name"
                                    className="font-semibold text-gray-700 block pb-1"
                                >
                                    Tài khoản
                                </label>
                                <div>
                                    <input
                                        disabled
                                        onChange={formik.handleChange}
                                        name="taiKhoan"
                                        id="username"
                                        className="border border-gray-400 rounded-r px-4 py-2 w-full cursor-no-drop"
                                        type="text"
                                        value={formik.values.taiKhoan}
                                    />
                                </div>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="font-semibold text-gray-700 block pb-1"
                                >
                                    Mật khẩu
                                </label>
                                <input
                                    onChange={formik.handleChange}
                                    name="matKhau"
                                    className="border border-gray-400 rounded-r px-4 py-2 w-full"
                                    value={formik.values.matKhau}
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.matKhau}
                                </span>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="font-semibold text-gray-700 block pb-1"
                                >
                                    Họ tên
                                </label>
                                <input
                                    onChange={formik.handleChange}
                                    name="hoTen"
                                    className="border border-gray-400 rounded-r px-4 py-2 w-full"
                                    value={formik.values.hoTen}
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.hoTen}
                                </span>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="font-semibold text-gray-700 block pb-1"
                                >
                                    Email
                                </label>
                                <input
                                    onChange={formik.handleChange}
                                    name="email"
                                    className="border border-gray-400 rounded-r px-4 py-2 w-full"
                                    value={formik.values.email}
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.email}
                                </span>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="font-semibold text-gray-700 block pb-1"
                                >
                                    Số điện thoại
                                </label>
                                <input
                                    onChange={formik.handleChange}
                                    name="soDt"
                                    className="border border-gray-400 rounded-r px-4 py-2 w-full"
                                    value={formik.values.soDt}
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.soDt}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
}
