import { useFormik } from "formik";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { postDangKyAction } from "../../redux/actions/QuanLyNguoiDungAction";
import { GROUP_ID } from "../../utils/settings/config";
import { signUpUserSchema } from "../../utils/validationSchema/userValidationSchema";

export default function Register() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            taiKhoan: "",
            matKhau: "",
            email: "",
            soDt: "",
            maNhom: `${GROUP_ID}`,
            hoTen: "",
        },
        onSubmit: (values) => {
            dispatch(postDangKyAction(values));
        },
        validationSchema: signUpUserSchema,
    });

    return (
        <form onSubmit={formik.handleSubmit} className="font-sans">
            <div className="relative py-10 flex flex-col sm:justify-center items-center  ">
                <div className="relative sm:max-w-sm w-full">
                    <div className="card bg-blue-400 shadow-lg  w-full h-full rounded-3xl absolute  transform -rotate-6" />
                    <div className="card bg-red-400 shadow-lg  w-full h-full rounded-3xl absolute  transform rotate-6" />
                    <div className="relative w-full rounded-3xl  px-6 py-4 bg-gray-100 shadow-md">
                        <label className="block mt-3 text-sm text-gray-700 text-center font-semibold">
                            ĐĂNG KÝ TÀI KHOẢN
                        </label>
                        <div method="#" action="#" className="mt-10">
                            <div>
                                <input
                                    name="taiKhoan"
                                    onChange={formik.handleChange}
                                    type="text"
                                    placeholder="Nhập tài khoản"
                                    className="mt-1 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none px-5"
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.taiKhoan}
                                </span>
                            </div>
                            <div className="mt-7">
                                <input
                                    name="matKhau"
                                    onChange={formik.handleChange}
                                    type="password"
                                    placeholder="Nhập mật khẩu"
                                    className="mt-1 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none px-5"
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.matKhau}
                                </span>
                            </div>
                            <div className="mt-7">
                                <input
                                    name="email"
                                    onChange={formik.handleChange}
                                    type="email"
                                    placeholder="Nhập email"
                                    className="mt-1 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none px-5"
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.email}
                                </span>
                            </div>
                            <div className="mt-7">
                                <input
                                    name="soDt"
                                    onChange={formik.handleChange}
                                    type="text"
                                    placeholder="Nhập số điện thoại"
                                    className="mt-1 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none px-5"
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.soDt}
                                </span>
                            </div>
                            <div className="mt-7">
                                <input
                                    onChange={formik.handleChange}
                                    name="hoTen"
                                    type="text"
                                    placeholder="Nhập họ tên"
                                    className="mt-1 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none px-5"
                                />
                                <span className="text-red-500 text-xs">
                                    {formik.errors.hoTen}
                                </span>
                            </div>
                            <div className="mt-7">
                                <button
                                    type="submit"
                                    className="bg-blue-500 w-full py-3 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105"
                                >
                                    ĐĂNG KÝ
                                </button>
                            </div>
                            <div className="mt-7">
                                <div className="flex justify-center items-center">
                                    <label className="mr-2">
                                        Bạn đã có tài khoản?
                                    </label>
                                    <NavLink
                                        to="/login"
                                        className=" text-blue-500 transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105"
                                    >
                                        Đăng nhập
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    );
}
