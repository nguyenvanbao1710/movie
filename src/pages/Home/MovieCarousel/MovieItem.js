import { PlayCircleOutlined } from "@ant-design/icons";
import { Card, Modal, Tooltip } from "antd";
import moment from "moment";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function MovieItem({ data }) {
    const [open, setOpen] = useState(false);
    return (
        <div className="group">
            <div>
                <Modal
                    centered
                    open={open}
                    onOk={() => setOpen(false)}
                    onCancel={() => setOpen(false)}
                    width={800}
                    footer={null}
                    destroyOnClose={true}
                    bodyStyle={{
                        padding: 0,
                        height: "500px",
                    }}
                >
                    <iframe
                        width="100%"
                        height="100%"
                        src={data.trailer}
                        title="YouTube video player"
                        frameBorder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                    ></iframe>
                </Modal>
            </div>
            <Card
                hoverable
                style={{
                    height: "100%",
                }}
                cover={
                    <div
                        className="group-hover:opacity-25"
                        style={{
                            background: `url(${data.hinhAnh}) no-repeat`,
                            backgroundPosition: "center",
                            backgroundSize: "cover",
                        }}
                    >
                        <img
                            style={{ height: "314px" }}
                            alt="example"
                            src={data.hinhAnh}
                            className="opacity-0 w-full"
                        />
                    </div>
                }
            >
                <div className="group-hover:opacity-20">
                    <Meta
                        className="break-words "
                        title={
                            <span className="lg:text-lg xs:text-base break-all">
                                {data.tenPhim}
                            </span>
                        }
                        description={
                            <span className="text-sm xs:text-xs font-medium text-red-600">
                                Ngày khởi chiếu{" "}
                                {moment(data.ngayKhoiChieu).format(
                                    "DD/MM/YYYY"
                                )}
                            </span>
                        }
                    />
                </div>
                <div
                    className="absolute w-full h-full opacity-0  mr-10 fd-sh group-hover:opacity-100   "
                    style={{ top: 0, left: 0 }}
                >
                    <div className="text-center w-full h-full grid grid-rows-4">
                        <div className="row-start-2 justify-end">
                            <button
                                onClick={() => setOpen(true)}
                                className="  text-center    hover:text-gray-400  text-gray-500 text-5xl"
                            >
                                <Tooltip title="Trailer">
                                    <PlayCircleOutlined />
                                </Tooltip>
                            </button>
                        </div>
                        <div className=" row-end-5 lg:mt-10 xs:mt-20">
                            <NavLink to={`/detail/${data.maPhim}`}>
                                <button className="w-full text-center rounded-lg lg:py-5 xs:py-2 sm:py5 xs:text-base bg-red-600 hover:bg-red-500  text-white font-bold text-xl">
                                    MUA VÉ
                                </button>
                            </NavLink>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
    );
}
