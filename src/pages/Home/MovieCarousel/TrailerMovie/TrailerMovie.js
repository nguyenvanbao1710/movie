import React, { useState } from "react";
import "./trailerMovie.css";
export default function TrailerMovie() {
    const [modal, setModal] = useState(false);
    const openModal = () => {
        setModal(!modal);
    };

    return (
        <>
            <button onClick={openModal}>
                {modal ? (
                    <section className="modal__bg">
                        <div className="modal__align">
                            <div className="modal__content" modal={modal}>
                                <button
                                    className="modal__close"
                                    arial-label="Close modal"
                                    onClick={setModal}
                                />
                                <div className="modal__video-align">
                                    <iframe
                                        className="modal__video-style"
                                        loading="lazy"
                                        width="800"
                                        height="500"
                                        src="https://www.youtube.com/embed/_nWfxFnPRj0"
                                        title="YouTube video player"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    ></iframe>
                                </div>
                            </div>
                        </div>
                    </section>
                ) : null}
            </button>
        </>
    );
}
