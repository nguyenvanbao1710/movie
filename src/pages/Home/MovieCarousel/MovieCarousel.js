import { Carousel } from "antd";
import React from "react";
import MovieItem from "./MovieItem";
import "./movieCarousel.css";

export default function MovieCarousel({ movieList }) {
    return (
        <div id="carousel_homepage">
            <Carousel>
                {movieList.map((item, index) => {
                    return (
                        <div className="h-max w-full pt-5 pb-14" key={index}>
                            <div className="grid lg:grid-cols-4 md:grid-cols-3 md:gap-5 lg:gap-10 xs:grid-cols-1 xs:gap-10 ">
                                {item.map((movie, index) => {
                                    return (
                                        <MovieItem data={movie} key={index} />
                                    );
                                })}
                            </div>
                        </div>
                    );
                })}
            </Carousel>
        </div>
    );
}
