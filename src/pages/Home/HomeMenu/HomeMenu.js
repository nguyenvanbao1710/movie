import React from "react";
import { Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";
import { NavLink } from "react-router-dom";
import moment from "moment";
import "../MovieCarousel/movieCarousel.css";
export default function HomeMenu({ heThongRapChieu }) {
    const renderHeThongRap = () => {
        return heThongRapChieu.map((rapChieu, index) => {
            return (
                <TabPane
                    tab={
                        <img
                            src={rapChieu.logo}
                            alt=""
                            className="rounded-full "
                            width="50"
                        />
                    }
                    key={index}
                >
                    <Tabs
                        tabPosition="left"
                        style={{ height: "500px", overflowY: "scroll" }}
                    >
                        {rapChieu.lstCumRap?.map((cumRap, index) => {
                            return (
                                <TabPane
                                    tab={
                                        <div className="w-96 whitespace-normal text-left border-b-2">
                                            <p className="text-lg text-green-600 font-medium">
                                                {cumRap.tenCumRap}
                                            </p>
                                            <p className="text-base text-red-600">
                                                [chi tiết]
                                            </p>
                                        </div>
                                    }
                                    key={index}
                                >
                                    {cumRap.danhSachPhim?.map((phim, index) => {
                                        return (
                                            <div
                                                key={index}
                                                className="flex py-1"
                                            >
                                                <div
                                                    style={{
                                                        background: `url(${phim.hinhAnh})`,
                                                        backgroundPosition:
                                                            "center",
                                                        backgroundSize: "cover",
                                                        width: "200px",
                                                    }}
                                                >
                                                    <img
                                                        style={{
                                                            width: "100%",
                                                            height: "126px",
                                                            opacity: "0",
                                                        }}
                                                        src={phim.hinhAnh}
                                                        alt=""
                                                    />
                                                </div>
                                                <div className="mx-4 border-b-2 w-full">
                                                    <div className="text-lg font-medium">
                                                        {phim.tenPhim}
                                                    </div>
                                                    <div className="text-sm font-medium">
                                                        {cumRap.diaChi}
                                                    </div>
                                                    <div>
                                                        {phim.lstLichChieuTheoPhim
                                                            ?.slice(0, 2)
                                                            .map(
                                                                (
                                                                    lichChieu,
                                                                    index
                                                                ) => {
                                                                    return (
                                                                        <NavLink
                                                                            to={`/checkout/${lichChieu.maLichChieu}`}
                                                                            className="flex"
                                                                            key={
                                                                                index
                                                                            }
                                                                        >
                                                                            <div className="flex border py-2 px-3 my-2 border-green-400">
                                                                                <div className="text-xs text-green-600 font-semibold mr-4 ">
                                                                                    {moment(
                                                                                        lichChieu.ngayChieuGioChieu
                                                                                    ).format(
                                                                                        "DD/MM/YYYY"
                                                                                    )}
                                                                                </div>
                                                                                <div className="text-xs text-red-600 font-semibold">
                                                                                    {moment(
                                                                                        lichChieu.ngayChieuGioChieu
                                                                                    ).format(
                                                                                        "hh:mm A"
                                                                                    )}
                                                                                </div>
                                                                            </div>
                                                                        </NavLink>
                                                                    );
                                                                }
                                                            )}
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </TabPane>
                            );
                        })}
                    </Tabs>
                </TabPane>
            );
        });
    };
    return (
        <div>
            <Tabs tabPosition="left">{renderHeThongRap()}</Tabs>
        </div>
    );
}
