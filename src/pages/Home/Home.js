import React, { useEffect } from "react";
import _ from "lodash";
import MovieCarousel from "./MovieCarousel/MovieCarousel";
import HomeMenu from "./HomeMenu/HomeMenu";
import { useDispatch, useSelector } from "react-redux";
import { getDanhSachPhimAction } from "../../redux/actions/QuanLyPhimAction";
import { getDanhSachHeThongRapAction } from "../../redux/actions/QuanLyRapAction";
import Banner from "../../templates/Layout/Banner/Banner";

export default function Home() {
    const dispatch = useDispatch();
    const { arrMovie } = useSelector((state) => state.MovieReducer);
    const chunkMovieList = _.chunk(arrMovie, 8);
    const { heThongRapChieu } = useSelector((state) => state.QuanLyRapReducer);
    useEffect(() => {
        dispatch(getDanhSachPhimAction);
        dispatch(getDanhSachHeThongRapAction);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <div>
            <Banner />
            <div className="mx-40 xs:mx-14 md:mx-28 ">
                <div className="my-10">
                    <MovieCarousel movieList={chunkMovieList} />
                </div>
                <div
                    className="lg:my-24 xs:my-0 xs:hidden  lg:block"
                    id="cumRap"
                >
                    <HomeMenu heThongRapChieu={heThongRapChieu} />
                </div>
            </div>
        </div>
    );
}
