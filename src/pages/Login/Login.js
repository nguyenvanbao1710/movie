import { useFormik } from "formik";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { postDangNhapAction } from "../../redux/actions/QuanLyNguoiDungAction";
import { signInUserSchema } from "../../utils/validationSchema/userValidationSchema";

export default function Login() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            taiKhoan: "",
            matKhau: "",
        },
        onSubmit: (values) => {
            dispatch(postDangNhapAction(values));
        },
        validationSchema: signInUserSchema,
    });

    return (
        <>
            <form onSubmit={formik.handleSubmit} className="flex-grow">
                <div className="font-sans">
                    <div className="relative py-10 flex flex-col  sm:justify-center items-center  ">
                        <div className="relative sm:max-w-sm w-full">
                            <div className="card bg-blue-400 shadow-lg  w-full h-full rounded-3xl absolute  transform -rotate-6" />
                            <div className="card bg-red-400 shadow-lg  w-full h-full rounded-3xl absolute  transform rotate-6" />
                            <div className="relative w-full rounded-3xl  px-6 py-4 bg-gray-100 shadow-md">
                                <label className="block mt-3 text-sm text-gray-700 text-center font-semibold">
                                    ĐĂNG NHẬP
                                </label>
                                <div method="#" action="#" className="mt-10">
                                    <div>
                                        <input
                                            name="taiKhoan"
                                            onChange={formik.handleChange}
                                            placeholder="Tài khoản"
                                            className="mt-1 px-5 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none"
                                        />
                                        <span className="text-red-500 text-xs">
                                            {formik.errors.taiKhoan}
                                        </span>
                                    </div>
                                    <div className="mt-7">
                                        <input
                                            name="matKhau"
                                            onChange={formik.handleChange}
                                            type="password"
                                            placeholder="Mật khẩu"
                                            className="mt-1 px-5 block w-full border-none bg-gray-100 h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus:outline-none"
                                        />
                                        <span className="text-red-500 text-xs">
                                            {formik.errors.matKhau}
                                        </span>
                                    </div>
                                    <div className="mt-7">
                                        <button className="bg-blue-500 w-full py-3 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
                                            ĐĂNG NHẬP
                                        </button>
                                    </div>
                                    <div className="mt-7">
                                        <div className="flex justify-center items-center">
                                            <label className="mr-2">
                                                Bạn chưa có tài khoản?
                                            </label>
                                            <NavLink
                                                to="/register"
                                                className=" text-blue-500 transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105"
                                            >
                                                Đăng ký tài khoản
                                            </NavLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    );
}
