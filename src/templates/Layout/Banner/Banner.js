import { Carousel } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getBannerAction } from "../../../redux/actions/QuanLyPhimAction";

export default function Banner(props) {
    const { arrBanner } = useSelector((state) => state.BannerReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBannerAction);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const contentStyle = {
        height: "65vh",
        color: "#fff",
        lineHeight: "160px",
        textAlign: "center",
        background: "#364d79",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
    };

    const renderBanner = () => {
        return arrBanner.map((item, index) => {
            return (
                <div key={index}>
                    <div
                        style={{
                            ...contentStyle,
                            backgroundImage: `url(${item.hinhAnh})`,
                        }}
                    >
                        <img
                            className="w-full opacity-0"
                            src={item.hinhAnh}
                            alt={item.hinhAnh}
                        />
                    </div>
                </div>
            );
        });
    };

    return (
        <Carousel effect="fade" autoplay autoplaySpeed={3000}>
            {renderBanner()}
        </Carousel>
    );
}
