import React from "react";
import logo from "../../../assets/img/logo.png";
import ios from "../../../assets/img/social-media/app-store.png";
import android from "../../../assets/img/social-media/google-play.png";
import facebook from "../../../assets/img/social-media/facebook.png";
import instagram from "../../../assets/img/social-media/instagram.png";
import twitter from "../../../assets/img/social-media/twitter.png";

export default function Footer() {
    return (
        <div id="lienHe">
            <footer className="px-4 divide-y bg-stone-900">
                <div className="container flex flex-col justify-between py-10 mx-auto space-y-8 lg:flex-row lg:space-y-0">
                    <div className="lg:w-1/3">
                        <a
                            rel="noopener noreferrer"
                            href="/"
                            className="flex justify-center space-x-3 "
                        >
                            <img src={logo} alt="" width={40} />
                            <span className=" self-center text-3xl font-bold text-red-600">
                                Cinema
                            </span>
                        </a>
                    </div>
                    <div className="grid grid-cols-2 text-lg gap-x-3 gap-y-8 lg:w-2/3 sm:grid-cols-4">
                        <div className="space-y-3">
                            <h3 className="tracking-wide uppercase text-base text-gray-300">
                                GIỚI THIỆU
                            </h3>
                            <ul className="space-y-1">
                                <li>
                                    <div className=" text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        VỀ CHÚNG TÔI
                                    </div>
                                </li>
                                <li>
                                    <div className="my-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        THOẢ THUẬN SỬ DỤNG
                                    </div>
                                </li>
                                <li>
                                    <div className="text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        QUY CHẾ HOẠT ĐỘNG
                                    </div>
                                </li>
                                <li>
                                    <div className="mt-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        CHÍNH SÁCH BẢO MẬT
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="space-y-3">
                            <h3 className="tracking-wide uppercase text-base text-gray-300">
                                GÓC ĐIỆN ẢNH
                            </h3>
                            <ul className="space-y-1">
                                <li>
                                    <div className=" text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        THỂ LOẠI PHIM
                                    </div>
                                </li>
                                <li>
                                    <div className="my-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        BÌNH LUẬN PHIM
                                    </div>
                                </li>
                                <li>
                                    <div className="text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        BLOG ĐIỆN ẢNH
                                    </div>
                                </li>
                                <li>
                                    <div className="mt-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        PHIM HAY THÁNG
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="space-y-3">
                            <h3 className="uppercase text-base text-gray-300">
                                HỖ TRỢ
                            </h3>
                            <ul className="space-y-1">
                                <li>
                                    <div className=" text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        GÓP Ý
                                    </div>
                                </li>
                                <li>
                                    <div className="my-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        SALE & SERVICES
                                    </div>
                                </li>
                                <li>
                                    <div className="text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        RẠP / GIÁ VÉ
                                    </div>
                                </li>
                                <li>
                                    <div className="mt-2 text-gray-300 hover:text-white text-xs cursor-pointer">
                                        <i className="fa fa-angle-double-right mr-1 "></i>
                                        TUYỂN DỤNG
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div className="space-y-3">
                            <h3 className="uppercase text-base text-gray-300">
                                KẾT NỐI CINEMA
                            </h3>
                            <div className="flex  justify-start space-x-3 cursor-pointer">
                                <div className="flex items-center p-1 ">
                                    <img src={facebook} alt="" width={30} />
                                </div>
                                <div className="flex items-center p-1 cursor-pointer">
                                    <img src={instagram} alt="" width={30} />
                                </div>
                                <div className="flex items-center p-1 cursor-pointer">
                                    <img src={twitter} alt="" width={30} />
                                </div>
                            </div>
                            <h3 className="uppercase text-base text-gray-300">
                                DOWNLOAD APP
                            </h3>
                            <div className="flex  justify-start space-x-3 cursor-pointer">
                                <div className="flex items-center p-1 ">
                                    <img src={ios} alt="" width={30} />
                                </div>
                                <div className="flex items-center p-1 cursor-pointer">
                                    <img src={android} alt="" width={30} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="py-6 text-xs text-center dark:text-gray-400">
                    © 2022 Cinema Company Co. All rights reserved.
                </div>
            </footer>
        </div>
    );
}
