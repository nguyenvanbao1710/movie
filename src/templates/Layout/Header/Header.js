/* eslint-disable jsx-a11y/anchor-is-valid */
import { Drawer } from "antd";
import _ from "lodash";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Link } from "react-scroll";
import logo from "../../../assets/img/logo.png";
import { TOKEN, USER_LOGIN } from "../../../utils/settings/config";

export default function Header() {
    const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
    const [open, setOpen] = useState(false);
    // eslint-disable-next-line no-unused-vars
    const [placement, setPlacement] = useState("right");
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };
    const handleLogout = () => {
        localStorage.removeItem(USER_LOGIN);
        localStorage.removeItem(TOKEN);
        window.location.href = "/";
    };
    const renderLogin = () => {
        if (_.isEmpty(userLogin)) {
            return (
                <Fragment>
                    <NavLink
                        to="/login"
                        className="xs:px-0 self-center px-3 py-2 rounded font-semibold text-black hover:text-red-600 text-xs"
                    >
                        <i className="fa-solid fa-user"></i>
                        <span className="pl-2">ĐĂNG NHẬP</span>
                    </NavLink>
                    <NavLink
                        to="/register"
                        className="self-center px-3 py-2 rounded font-semibold text-black hover:text-red-600 text-xs"
                    >
                        <i className="fa-solid fa-user"></i>
                        <span className="pl-2">ĐĂNG KÝ</span>
                    </NavLink>
                </Fragment>
            );
        }
        return (
            <Fragment>
                <NavLink
                    to="/account"
                    className="self-center px-3 py-2 rounded font-semibold text-black hover:text-red-600 text-xs"
                >
                    {userLogin?.hoTen}
                </NavLink>
                <button
                    onClick={handleLogout}
                    className=" self-center  py-2 rounded font-semibold text-black hover:text-red-600 text-xs"
                >
                    <i className="fa-solid fa-right-from-bracket"></i>
                </button>
            </Fragment>
        );
    };
    return (
        <div>
            <Drawer
                placement={placement}
                closable={true}
                onClose={onClose}
                open={open}
                key={placement}
                width={250}
            >
                <div className="flex flex-col  items-start ">
                    <div>{renderLogin()}</div>
                    <div className="pt-10">
                        <Link
                            spy={true}
                            smooth={true}
                            offset={50}
                            duration={500}
                            to="carousel_homepage"
                            className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                        >
                            Lịch chiếu
                        </Link>
                    </div>
                    <div className="py-10">
                        <Link
                            spy={true}
                            smooth={true}
                            offset={50}
                            duration={500}
                            to="cumRap"
                            className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                        >
                            Cụm rạp
                        </Link>
                    </div>
                    <div>
                        <Link
                            spy={true}
                            smooth={true}
                            offset={50}
                            duration={500}
                            to="lienHe"
                            className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                        >
                            Liên hệ
                        </Link>
                    </div>
                </div>
            </Drawer>
            <header className=" bg-white border-b-4  bg-opacity-90 fixed w-full z-10 ">
                <div className="container flex sm:px-10 xs:px-5  justify-between mx-auto lg:px-10 h-16  ">
                    <NavLink
                        to="/"
                        className="flex items-center p-2 text-3xl font-bold text-red-600 hover:text-red-600"
                    >
                        <img className="mr-2 " src={logo} alt="" width={40} />
                        Cinema
                    </NavLink>
                    <ul className="items-stretch hidden space-x-3 lg:flex mb-0">
                        <li className="flex items-center">
                            <Link
                                spy={true}
                                smooth={true}
                                offset={50}
                                duration={500}
                                to="carousel_homepage"
                                className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                            >
                                Lịch chiếu
                            </Link>
                        </li>
                        <li className="flex items-center">
                            <Link
                                spy={true}
                                smooth={true}
                                offset={50}
                                duration={500}
                                to="cumRap"
                                className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                            >
                                Cụm rạp
                            </Link>
                        </li>
                        <li className="flex items-center">
                            <Link
                                spy={true}
                                smooth={true}
                                offset={50}
                                duration={500}
                                to="lienHe"
                                className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                            >
                                Liên hệ
                            </Link>
                        </li>
                        <li className="flex items-center">
                            {userLogin.maLoaiNguoiDung === "QuanTri" ? (
                                <NavLink
                                    spy={true}
                                    smooth={true}
                                    offset={50}
                                    duration={500}
                                    to="/admin/users"
                                    className="flex items-center px-4 font-medium text-black hover:text-red-600 text-lg"
                                >
                                    Quản lý
                                </NavLink>
                            ) : (
                                ""
                            )}
                        </li>
                    </ul>
                    <div className="items-center flex-shrink-0 hidden lg:flex">
                        {renderLogin()}
                    </div>
                    <button className=" lg:hidden text-xl" onClick={showDrawer}>
                        <i className="fa-sharp fa-solid fa-bars"></i>
                    </button>
                </div>
            </header>
        </div>
    );
}
