import { Redirect, Route } from "react-router";
import { USER_LOGIN } from "../../utils/settings/config";
import Header from "../Layout/Header/Header";

const CheckoutTemplate = (props) => {
    const { Component, ...restRoute } = props;

    if (!localStorage.getItem(USER_LOGIN)) {
        return <Redirect to="/login" />;
    }

    return (
        <Route
            {...restRoute}
            render={(propsRoute) => {
                return (
                    <div
                        style={{
                            minHeight: "100vh",
                            display: "flex",
                            flexDirection: "column",
                        }}
                    >
                        <div className="pb-16">
                            <Header {...propsRoute} />
                        </div>
                        <div className="flex-grow">
                            <Component {...propsRoute} />
                        </div>
                    </div>
                );
            }}
        />
    );
};

export default CheckoutTemplate;
