import { Route } from "react-router";

export const UserTemplate = (props) => {
    const { Component, ...restRoute } = props;
    return (
        <Route
            {...restRoute}
            render={(propsRoute) => {
                return (
                    <div
                        style={{
                            minHeight: "100vh",
                            display: "flex",
                            flexDirection: "column",
                        }}
                    >
                        <Component {...propsRoute} />
                    </div>
                );
            }}
        />
    );
};
