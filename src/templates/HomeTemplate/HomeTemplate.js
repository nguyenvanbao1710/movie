import { Route } from "react-router";
import Footer from "../Layout/Footer/Footer";
import Header from "../Layout/Header/Header";

export const HomeTemplate = (props) => {
    const { Component, ...restRoute } = props;
    return (
        <Route
            {...restRoute}
            render={(propsRoute) => {
                return (
                    <div
                        style={{
                            minHeight: "100vh",
                            display: "flex",
                            flexDirection: "column",
                        }}
                    >
                        <div className="pb-16">
                            <Header {...propsRoute} />
                        </div>
                        <div className="flex-grow">
                            <Component {...propsRoute} />
                        </div>
                        <div>
                            <Footer />
                        </div>
                    </div>
                );
            }}
        />
    );
};
