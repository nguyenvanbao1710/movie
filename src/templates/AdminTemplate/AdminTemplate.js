import { Redirect, Route } from "react-router";
import { USER_LOGIN } from "../../utils/settings/config";
import HeaderHome from "../Layout/Header/Header";
import { DesktopOutlined, FileOutlined, UserOutlined } from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, message } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import SubMenu from "antd/lib/menu/SubMenu";
const { Content, Sider } = Layout;

const AdminTemplate = (props) => {
    const [collapsed, setCollapsed] = useState(false);

    const onCollapse = (collapsed) => {
        setCollapsed(collapsed);
    };
    const { Component, ...restRoute } = props;
    const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);

    if (!localStorage.getItem(USER_LOGIN)) {
        message.error("Bạn không có quyền truy cập trang này!");
        return <Redirect to="/" />;
    }
    if (userLogin.maLoaiNguoiDung !== "QuanTri") {
        message.error("Bạn không có quyền truy cập trang này!");
        return <Redirect to="/" />;
    }

    return (
        <Route
            {...restRoute}
            render={(propsRoute) => {
                return (
                    <div>
                        <div className="pb-16">
                            <HeaderHome {...propsRoute} />
                        </div>
                        <div>
                            <Layout style={{ minHeight: "85vh" }}>
                                <Sider
                                    collapsible
                                    collapsed={collapsed}
                                    onCollapse={onCollapse}
                                >
                                    <Menu
                                        theme="dark"
                                        defaultSelectedKeys={["1"]}
                                        mode="inline"
                                    >
                                        <Menu.Item
                                            key="1"
                                            icon={<UserOutlined />}
                                        >
                                            <NavLink to="/admin/users">
                                                Users
                                            </NavLink>
                                        </Menu.Item>
                                        <Menu.Item
                                            key="10"
                                            icon={<FileOutlined />}
                                        >
                                            <NavLink to="/admin/films">
                                                Films
                                            </NavLink>
                                        </Menu.Item>
                                    </Menu>
                                </Sider>
                                <Layout className="bg-white">
                                    <Content style={{ margin: "0 16px" }}>
                                        <Breadcrumb
                                            style={{ margin: "16px 0" }}
                                        ></Breadcrumb>
                                        <div
                                            style={{
                                                minHeight: "85vh",
                                            }}
                                        >
                                            <Component {...propsRoute} />
                                        </div>
                                    </Content>
                                </Layout>
                            </Layout>
                        </div>
                    </div>
                );
            }}
        />
    );
};

export default AdminTemplate;
