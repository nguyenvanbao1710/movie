import * as yup from "yup";

export const addNewSchema = yup.object().shape({
    tenPhim: yup.string().required("* Bạn chưa nhập tên phim!"),
    trailer: yup.string().required("* Bạn chưa nhập trailer!"),
    moTa: yup.string().required("* Bạn chưa nhập mô tả!"),
    ngayKhoiChieu: yup.string().required("* Bạn chưa nhập ngày khởi chiếu!"),
});

export const showTimeSchema = yup.object().shape({
    ngayChieuGioChieu: yup.string().required("*Bạn chưa chọn ngày giờ chiếu!"),
    maRap: yup.string().required("*Bạn chưa chọn rạp!"),
    giaVe: yup.string().required("*Bạn chưa nhập giá vé!"),
});
