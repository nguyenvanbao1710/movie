import * as yup from "yup";

export const signUpUserSchema = yup.object().shape({
    taiKhoan: yup.string().required("* Bạn chưa nhập tài khoản!"),
    matKhau: yup
        .string()
        .matches(/^[0-9a-zA-Z]{6,}$/, "* Mật khẩu chứa ít nhất 6 kí tự!")
        .required("* Bạn chưa nhập mật khẩu!"),
    email: yup
        .string()
        .required("* Bạn chưa nhập email!")
        .email("* Email không hợp lệ!"),
    soDt: yup
        .string()
        .matches(
            // eslint-disable-next-line no-useless-escape
            /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
            "* Số điện thoại không hợp lệ!"
        )
        .required("* Bạn chưa nhập số điện thoại!"),
    hoTen: yup.string().required("* Bạn chưa nhập họ tên!"),
});

export const signInUserSchema = yup.object().shape({
    taiKhoan: yup.string().required("* Bạn chưa nhập tài khoản!"),
    matKhau: yup
        .string()
        .matches(/^[0-9a-zA-Z]{6,}$/, "* Mật khẩu chứa ít nhất 6 kí tự!")
        .required("* Bạn chưa nhập mật khẩu!"),
});
