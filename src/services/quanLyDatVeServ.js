import axios from "axios";
import {
    AUTHORIZATION,
    BASE_URL,
    TOKEN_CYBERSOFT,
} from "../utils/settings/config";
import { ThongTinDatVe } from "../_core/models/ThongTinDatVe";

export const quanLyDatVeServ = {
    getListPhongVe: (maLichChieu) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            method: "GET",
        });
    },
    datVe: (thongTinDatVe = new ThongTinDatVe()) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            data: thongTinDatVe,
            method: "POST",
        });
    },
    taoLichChieu: (thongTinLichChieu) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyDatVe/TaoLichChieu`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            data: thongTinLichChieu,
            method: "POST",
        });
    },
};
