import axios from "axios";
import {
    AUTHORIZATION,
    BASE_URL,
    GROUP_ID,
    TOKEN_CYBERSOFT,
} from "../utils/settings/config";
export const bannerService = {
    getBanner: () => {
        return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`, {
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
        });
    },
};
export const movieService = {
    getMovieList: () => {
        return axios.get(
            `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUP_ID}`,
            {
                headers: {
                    TokenCybersoft: TOKEN_CYBERSOFT,
                },
            }
        );
    },
    postPhimUploadHinh: (formData) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/ThemPhimUploadHinh`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            data: formData,
            method: "POST",
        });
    },
    getInforMovie: (maPhim) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            method: "GET",
        });
    },
    capNhatPhimUpLoad: (formData) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/CapNhatPhimUpload`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            data: formData,
            method: "POST",
        });
    },
    xoaPhim: (maPhim) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            method: "DELETE",
        });
    },
};
