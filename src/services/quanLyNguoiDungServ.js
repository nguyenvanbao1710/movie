import axios from "axios";
import {
    AUTHORIZATION,
    BASE_URL,
    GROUP_ID,
    TOKEN_CYBERSOFT,
} from "../utils/settings/config";

export const quanLyNguoiDungServ = {
    postDangNhap: (dataLogin) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            data: dataLogin,
            method: "POST",
        });
    },
    postDangKy: (dataSignUp) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            data: dataSignUp,
            method: "POST",
        });
    },

    layThongTinUser: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            method: "POST",
        });
    },
    updateThongTinUser: (dataUpdate) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            method: "PUT",
            data: dataUpdate,
        });
    },
    getUserList: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachNguoiDung`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
            method: "GET",
        });
    },
    deleteUser: (taiKhoan) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION,
            },
            method: "DELETE",
        });
    },
};
