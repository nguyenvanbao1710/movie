import axios from "axios";
import { BASE_URL, GROUP_ID, TOKEN_CYBERSOFT } from "../utils/settings/config";
export const quanLyRapService = {
    getListRap: () => {
        return axios.get(
            `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUP_ID}`,
            {
                headers: {
                    TokenCybersoft: TOKEN_CYBERSOFT,
                },
            }
        );
    },
    getInforLichChieu: (maPhim) => {
        return axios.get(
            `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`,
            {
                headers: {
                    TokenCybersoft: TOKEN_CYBERSOFT,
                },
            }
        );
    },
    getInforHeThongRap: () => {
        return axios.get(`${BASE_URL}/api/QuanLyRap/LayThongTinHeThongRap`, {
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
        });
    },
    layThongTinCumRap: (maHeThongRap) => {
        return axios.get(
            `${BASE_URL}/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`,
            {
                headers: {
                    TokenCybersoft: TOKEN_CYBERSOFT,
                },
            }
        );
    },
};
