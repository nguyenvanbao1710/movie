import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { BannerReducer } from "./reducers/BannerReducer";
import { LoadingReducer } from "./reducers/LoadingReducer";
import { MovieReducer } from "./reducers/MovieReducer";
import { QuanLyDatVeReducer } from "./reducers/QuanLyDatVeReducer";
import { QuanLyNguoiDungReducer } from "./reducers/QuanLyNguoiDungReducer";
import { QuanLyRapReducer } from "./reducers/QuanLyRapReducer";

const rootReducer = combineReducers({
    BannerReducer,
    MovieReducer,
    QuanLyRapReducer,
    QuanLyNguoiDungReducer,
    QuanLyDatVeReducer,
    LoadingReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
