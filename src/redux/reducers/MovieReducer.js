import {
    GET_MOVIE_LIST,
    SET_INFOR_MOVIE,
} from "../constants/quanLyPhimContants";
import { SET_CHI_TIET_PHIM } from "../constants/quanLyRapConstants";
const stateDefault = {
    arrMovie: [],
    filmDetail: {},
    thongTinFilm: {},
};

export const MovieReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case GET_MOVIE_LIST: {
            state.arrMovie = action.arrMovie;
            return { ...state };
        }
        case SET_CHI_TIET_PHIM: {
            state.filmDetail = action.filmDetail;
            return { ...state };
        }
        case SET_INFOR_MOVIE: {
            state.thongTinFilm = action.thongTinFilm;
            return { ...state };
        }
        default:
            return { ...state };
    }
};
