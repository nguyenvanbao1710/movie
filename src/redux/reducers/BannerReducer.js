import { SET_BANNER } from "../constants/bannerConstants";

const stateDefault = {
    arrBanner: [],
};

export const BannerReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case SET_BANNER: {
            state.arrBanner = action.arrBanner;
            return { ...state };
        }
        default:
            return { ...state };
    }
};
