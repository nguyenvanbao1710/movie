import { TOKEN, USER_LOGIN } from "../../utils/settings/config";
import {
    DANG_NHAP_ACTION,
    SET_THONG_TIN_NGUOI_DUNG,
} from "../constants/quanLyNguoiDungContants";
let user = {};
if (localStorage.getItem(USER_LOGIN)) {
    user = JSON.parse(localStorage.getItem(USER_LOGIN));
}
const stateDefault = {
    userLogin: user,
    thongTinNguoiDung: {},
    userUpdate: {},
};
export const QuanLyNguoiDungReducer = (state = stateDefault, action) => {
    switch (action.type) {
        case DANG_NHAP_ACTION: {
            const { dataLogin } = action;
            localStorage.setItem(USER_LOGIN, JSON.stringify(dataLogin));
            localStorage.setItem(TOKEN, dataLogin.accessToken);
            return { ...state, userLogin: dataLogin };
        }
        case SET_THONG_TIN_NGUOI_DUNG: {
            state.thongTinNguoiDung = action.thongTinNguoiDung;
            return { ...state };
        }
        default:
            return { ...state };
    }
};
