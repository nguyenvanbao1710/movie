import { message } from "antd";
import { quanLyDatVeServ } from "../../services/quanLyDatVeServ";
import {
    DAT_VE_HOAN_TAT,
    SET_CHI_TIET_PHONG_VE,
} from "../constants/quanLyDatVeConstants";
import { displayLoadingAction, hideLoadingAction } from "./LoadingActions";

export const getListPhongVeAction = (maLichChieu) => {
    return async (dispatch) => {
        try {
            const result = await quanLyDatVeServ.getListPhongVe(maLichChieu);
            if (result.statusText === "OK") {
                dispatch({
                    type: SET_CHI_TIET_PHONG_VE,
                    chiTietPhongVe: result.data.content,
                });
            }
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};

export const datVeAction = (thongTinDatVe) => {
    if (thongTinDatVe.danhSachVe.length === 0) {
        message.error("Bạn chưa chọn ghế!");
    } else {
        return async (dispatch) => {
            try {
                dispatch(displayLoadingAction);
                message.success(
                    "Đặt vé thành công! Vui lòng kiểm tra lịch sử."
                );
                // eslint-disable-next-line no-unused-vars
                const result = await quanLyDatVeServ.datVe(thongTinDatVe);
                await dispatch(getListPhongVeAction(thongTinDatVe.maLichChieu));
                await dispatch({ type: DAT_VE_HOAN_TAT });
                await dispatch(hideLoadingAction);
            } catch (error) {
                message.error(error.response.data.content);
                dispatch(hideLoadingAction);
            }
        };
    }
};
