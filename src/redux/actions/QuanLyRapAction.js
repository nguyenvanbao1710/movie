import { message } from "antd";
import { quanLyRapService } from "../../services/quanLyRapService";
import {
    SET_CHI_TIET_PHIM,
    SET_HE_THONG_RAP,
} from "../constants/quanLyRapConstants";

export const getDanhSachHeThongRapAction = async (dispatch) => {
    try {
        const result = await quanLyRapService.getListRap();
        dispatch({
            type: SET_HE_THONG_RAP,
            heThongRapChieu: result.data.content,
        });
    } catch (err) {
        message.error(err.response.data.content);
    }
};
export const layThongTinChiTietPhim = (id) => {
    return async (dispatch) => {
        try {
            const result = await quanLyRapService.getInforLichChieu(id);
            dispatch({
                type: SET_CHI_TIET_PHIM,
                filmDetail: result.data.content,
            });
        } catch (err) {
            message.error(err.response.data.content);
        }
    };
};
