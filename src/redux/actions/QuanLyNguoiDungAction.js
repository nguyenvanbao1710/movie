import { message } from "antd";
import { history } from "../../App";
import { quanLyNguoiDungServ } from "../../services/quanLyNguoiDungServ";
import {
    DANG_NHAP_ACTION,
    SET_THONG_TIN_NGUOI_DUNG,
} from "../constants/quanLyNguoiDungContants";
import { displayLoadingAction, hideLoadingAction } from "./LoadingActions";
export const postDangNhapAction = (dataLogin) => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungServ.postDangNhap(dataLogin);
            if (result.data.statusCode === 200) {
                dispatch({
                    type: DANG_NHAP_ACTION,
                    dataLogin: result.data.content,
                });
            }
            window.location.href = "/";
            message.success("Đăng nhập thành công!");
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};
export const postDangKyAction = (dataSignUp) => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungServ.postDangKy(dataSignUp);
            message.success("Đăng ký thành công!");
            history.goBack();
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};
export const layThongTinNguoiDungAction = () => {
    return async (dispatch) => {
        try {
            dispatch(displayLoadingAction);
            const result = await quanLyNguoiDungServ.layThongTinUser();
            if (result.data.statusCode === 200) {
                dispatch({
                    type: SET_THONG_TIN_NGUOI_DUNG,
                    thongTinNguoiDung: result.data.content,
                });
            }
            dispatch(hideLoadingAction);
        } catch (error) {
            dispatch(hideLoadingAction);
        }
    };
};
export const updateThongTinUserAction = (dataUpdate) => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungServ.updateThongTinUser(
                dataUpdate
            );
            message.success("Cập nhật thành công!");
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};
export const deleteUserAction = (taiKhoan) => {
    return async (dispatch) => {
        try {
            const result = await quanLyNguoiDungServ.deleteUser(taiKhoan);
            dispatch(quanLyNguoiDungServ.getUserList());
            message.success("Xoá tài khoản thành công!");
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};
