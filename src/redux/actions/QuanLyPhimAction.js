import { message } from "antd";
import { bannerService, movieService } from "../../services/movieService";
import {
    GET_MOVIE_LIST,
    SET_INFOR_MOVIE,
} from "../constants/quanLyPhimContants";
import { history } from "../../App";
import { SET_BANNER } from "../constants/bannerConstants";

export const getBannerAction = async (dispatch) => {
    try {
        const result = await bannerService.getBanner();
        dispatch({
            type: SET_BANNER,
            arrBanner: result.data.content,
        });
    } catch (err) {
        message.error(err.response.data.content);
    }
};

export const getDanhSachPhimAction = async (dispatch) => {
    try {
        const result = await movieService.getMovieList();
        dispatch({
            type: GET_MOVIE_LIST,
            arrMovie: result.data.content,
        });
    } catch (err) {
        message.error(err.response.data.content);
    }
};

export const postPhimUploadHinh = (formData) => {
    return async (dispatch) => {
        try {
            const result = await movieService.postPhimUploadHinh(formData);
            message.success("Thêm phim thành công!");
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};

export const getInforMovieAction = (maPhim) => {
    return async (dispatch) => {
        try {
            const result = await movieService.getInforMovie(maPhim);
            dispatch({
                type: SET_INFOR_MOVIE,
                thongTinFilm: result.data.content,
            });
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};

export const postCapNhatPhimUpLoadAction = (formData) => {
    return async (dispatch) => {
        try {
            const result = await movieService.capNhatPhimUpLoad(formData);
            message.success("Cập nhật phim thành công!");
            dispatch(getDanhSachPhimAction);
            history.goBack();
        } catch (error) {
            message.error(error.response.data.content);
        }
    };
};

export const xoaPhimAction = (maPhim) => {
    return async (dispatch) => {
        try {
            const result = await movieService.xoaPhim(maPhim);
            dispatch(getDanhSachPhimAction);
            message.success("Xoá phim thành công!");
        } catch (error) {
            message.error("Hệ thống đang gặp sự cố!");
        }
    };
};
